Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
GET | /customers/all | | [{"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"}, ...] | Returns array of customer objects for all customers in the database
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | *customerNumber* | Returns customer with given number
PUT | /customers/*number*/change | {"firstName": "*firstName*", "lastName": "*lastName*" } | | Changes the name of a customer in the database
GET | /customers/number | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Returns customer number given customer first name and last name
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
GET | /orders/all/*customerNumber* | | [{*orderNumber*}, ...] | Returns an array of all a customer's order numbers given customer number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
