package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    } 

    /**
     * Get all customers in the database
     * @return An array of all of the customers in the database
     */
    @GetMapping("/customers/all")
    public ResponseEntity<Object> getAllCustomers() {

        Customer[] customerArray = new Customer[ (int) counter.get()]; //create an array of type customer with the size of the amount of customers in the database

        //fill array with customers by customer number
        for (long i = 0; i < counter.get(); i++ )
        {
            customerArray[ (int) i] = customerDb.get(i+1); 
        }
        
        return new ResponseEntity<>(customerArray, HttpStatus.OK);
    }
       

    /**
     * Change a customer's name in the database given the customer number
     * @param number the customer number
     * @param customer the customer's new first name and last name
     * @return whether the customer name was succesfully changed or if the customer was not found in the DB
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PutMapping("/customers/{number}/change")
    public ResponseEntity<Object> changeCustomerName(@PathVariable long number, @RequestBody Customer customer) {
        if (customerDb.containsKey(number)) {
            customerDb.get(number).setFirstName(customer.getFirstName());
            customerDb.get(number).setLastName(customer.getLastName());
            return new ResponseEntity<>("Successfully updated customer information", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
        }
    }


     /**
     * Get a customer's number given first name and last name
     * @param customer the customer's first and last names to search the database for
     * @return the customer's number, if they are in the database, otherwise not found
     */
    @GetMapping("/customers/number")
    public ResponseEntity<Object> getCustomerNumber(@RequestBody Customer customer) {
        long i = 1;

        //search database for customer starting at first key value to maximum customer number
        while (i <= counter.get())
        {
            //compare input customer first and last name to customer's in database based on customer number
            if ( (customerDb.get(i).getFirstName().equals(customer.getFirstName()) ) && (customerDb.get(i).getLastName().equals(customer.getLastName()) ) ) {
                return new ResponseEntity<>(i, HttpStatus.OK);
            }
            i++;
        }

        return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);

    }
   

}